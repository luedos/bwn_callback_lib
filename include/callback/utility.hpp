#pragma once

namespace bwn
{
	template<typename ToTypeT, typename FromTypeT>
	inline ToTypeT union_cast(const FromTypeT& p_obj)
	{
		// static_assert(sizeof(ToTypeT) == sizeof(FromTypeT), "The sizes of two casted objects must be equal, otherwise this could be even more ub then it already is!");
		union
		{
			ToTypeT castedObject;
			FromTypeT originalObject;
		} conversion;

		conversion.originalObject = p_obj;
		return conversion.castedObject;
	}
} // namespace bwn
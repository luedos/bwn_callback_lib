#pragma once

#include "callback/callback.hpp"

template<typename ReturnT, typename...ArgTs>
template<typename LocalClassT>
void bwn::callback_detail::BasicCallable<ReturnT(ArgTs...)>::set(LocalClassT* _obj, ReturnT(LocalClassT::*_method)(ArgTs...))
{
	// Ok, this is very dangerous ub, don't do it at home.
	struct PlainThisGetter
	{
		UnknownType* get()
		{
			// Basically, anything will be passed here as 'this' will be returned as UnknownType*
			return (UnknownType*)(void*)(this);
		}
	};

	union
	{
		UnknownType*(PlainThisGetter::*realGetThis)(); // This will override function pointer.
		UnknownType*(LocalClassT::*masskedGetThis)(); // This is just combination of two things above.
		ReturnT(LocalClassT::*method)(ArgTs...); // This will give us all neccecery info beside simple function pointer.
	} conversion;

	conversion.method = _method;
	conversion.realGetThis = &PlainThisGetter::get;

	m_this = (_obj->*conversion.masskedGetThis)();
	m_method = bwn::union_cast<void*>(_method);
}

template<typename ReturnT, typename...ArgTs>
template<typename LocalClassT>
void bwn::callback_detail::BasicCallable<ReturnT(ArgTs...)>::set(const LocalClassT* _obj, ReturnT(LocalClassT::*_method)(ArgTs...) const)
{
	set(const_cast<LocalClassT*>(_obj), bwn::union_cast<ReturnT(LocalClassT::*)(ArgTs...)>(_method));
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback()
{
	initialize_empty();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(ReturnT(*_functionPtr)(ArgTs...))
{
	if (_functionPtr == nullptr)
	{
		initialize_empty();
		return;
	}

	struct FunctionCaller
	{
		ReturnT call(ArgTs..._args) const
		{
			return ((FunctionPrototype)(void*)(this))(std::forward<ArgTs>(_args)...);
		}
	};

	m_callable.set((FunctionCaller*)(void*)(_functionPtr), &FunctionCaller::call);
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(std::nullptr_t)
	: basic_callback()
{}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<typename LocalClassT>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(LocalClassT* _obj, ReturnT(LocalClassT::*_methodPtr)(ArgTs...))
{
	if (_methodPtr == nullptr)
	{
		initialize_empty();
		return;
	}

	assert(_obj != nullptr && "This is not safe to pass to callback non null method pointer, but nullptr for the object.");

	m_callable.set(_obj, _methodPtr);
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<typename LocalClassT>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(const LocalClassT* _obj, ReturnT(LocalClassT::*_methodPtr)(ArgTs...) const)
{
	if (_methodPtr == nullptr)
	{
		initialize_empty();
		return;
	}

	assert(_obj != nullptr && "This is not safe to pass to callback non null method pointer, but nullptr for the object.");

	m_callable.set(_obj, _methodPtr);
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<typename LocalFunctorT, typename>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(LocalFunctorT&& _functor)
{
	using Functor = typename std::remove_reference<LocalFunctorT>::type;
	using LocalFunctorWrapper = callback_detail::DerivedFunctorWrapper<SignatureType, Functor>;

	void*const ptr = allocate(sizeof(LocalFunctorWrapper));
	LocalFunctorWrapper*const wrapper = new (ptr) LocalFunctorWrapper(std::forward<LocalFunctorT>(_functor));

	m_callable.set(&wrapper->m_functor, &Functor::operator());
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(const basic_callback& _other)
{
	if (!_other.is_valid())
	{
		initialize_empty();
		return;
	}

	if (_other.has_functor())
	{
		const callback_detail::BaseFunctorWrapper<SignatureType>*const wrapper = _other.get_functor();
		void*const memory = allocate(wrapper->getSizeV());
		wrapper->cloneIntoV(memory, m_callable);
	}
	else
	{
		m_callable.m_this = _other.m_callable.m_this;
		m_callable.m_method = _other.m_callable.m_method;
		initialize_empty_buffer();
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(basic_callback&& _other) noexcept
{
	initialize_empty();
	swap(_other);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(const basic_callback<ReturnT(ArgTs...), k_otherBufferSize>& _other)
{
	if (!_other.is_valid())
	{
		initialize_empty();
		return;
	}

	if (_other.has_functor())
	{
		const callback_detail::BaseFunctorWrapper<SignatureType>*const wrapper = _other.get_functor();
		void*const memory = allocate(wrapper->getSizeV());
		wrapper->cloneIntoV(memory, m_callable);
	}
	else
	{
		m_callable.m_this = _other.m_callable.m_this;
		m_callable.m_method = _other.m_callable.m_method;
		initialize_empty_buffer();
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::basic_callback(basic_callback<ReturnT(ArgTs...), k_otherBufferSize>&& _other) noexcept
{
	initialize_empty();
	swap(_other);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::~basic_callback()
{
	clear();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	const basic_callback& _other)
{
	if (&_other == this)
	{
		return *this;
	}

	basic_callback clonedCallback = _other;
	swap(clonedCallback);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	basic_callback&& _other) noexcept
{
	if (&_other == this)
	{
		return *this;
	}

	clear();
	swap(_other);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	const basic_callback<ReturnT(ArgTs...),k_otherBufferSize>& _other)
{
	basic_callback clonedCallback = _other;
	swap(clonedCallback);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>& bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator=(
	basic_callback<ReturnT(ArgTs...),k_otherBufferSize>&& _other) noexcept
{
	clear();
	swap(_other);

	return *this;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_equal(const basic_callback<ReturnT(ArgTs...),k_otherBufferSize>& _other) const
{
	const bool isFunctor = has_functor();

	if (isFunctor != _other.has_functor())
	{
		// If we hold functor but other does not, or in reverse, where is no point in comparison.
		return false;
	}

	if (isFunctor)
	{
		const callback_detail::BaseFunctorWrapper<SignatureType>*const selfWrapper = get_functor();
		const callback_detail::BaseFunctorWrapper<SignatureType>*const otherWrapper = _other.get_functor();

		return selfWrapper->isEqualV(*otherWrapper);
	}

	// If this is not a functor comparison, then we have more or less no other option, other then to just compare function and class pointers.
	return m_callable.m_this == _other.m_callable.m_this
		&& m_callable.m_method == _other.m_callable.m_method;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator bool() const
{
	return is_valid();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_valid() const
{
	// theoretically m_this could equal to 0, and still be called correctly,
	// but if m_method == nullptr, that's mean we simply not initialized basic_callback yet.
	return m_callable.m_method != nullptr;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
ReturnT bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::operator()(ArgTs..._args) const
{
	assert(is_valid() && "Calling empty callback! Why?");

	union
	{
		MethodPrototype method;
		void* voidPtr;
	} conversion;
	conversion.method = 0;
	conversion.voidPtr = m_callable.m_method;

	return ((m_callable.m_this)->*conversion.method)(std::forward<ArgTs>(_args)...);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
ReturnT bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::invoke(ArgTs..._args) const
{
	assert(is_valid() && "Calling empty callback! Why?");

	union
	{
		MethodPrototype method;
		void* voidPtr;
	} conversion;
	conversion.method = 0;
	conversion.voidPtr = m_callable.m_method;

	return ((m_callable.m_this)->*conversion.method)(std::forward<ArgTs>(_args)...);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::swap(basic_callback& _other) noexcept
{
	const bool selfLocal = is_local(m_callable.m_this);
	const bool otherLocal = _other.is_local(_other.m_callable.m_this);

	if (otherLocal && selfLocal)
	{
		size_t swapBuffer[k_totalBufferCount];
		callback_detail::BasicCallable<SignatureType> swapCallable;
		_other.get_functor()->cloneIntoV(swapBuffer, swapCallable);
		_other.get_functor()->~BaseFunctorWrapper();

		get_functor()->cloneIntoV(_other.m_buffer, _other.m_callable);
		get_functor()->~BaseFunctorWrapper();
		((callback_detail::BaseFunctorWrapper<SignatureType>*)swapBuffer)->cloneIntoV(m_buffer, m_callable);
	}
	else if (otherLocal)
	{
		const callback_detail::BasicCallable<SignatureType> oldCallable = m_callable;
		const size_t oldFirstWord = m_buffer[0];

		_other.get_functor()->cloneIntoV(m_buffer, m_callable);
		_other.get_functor()->~BaseFunctorWrapper();

		_other.m_callable = oldCallable;
		_other.m_buffer[0] = oldFirstWord;
	}
	else if (selfLocal)
	{
		const callback_detail::BasicCallable<SignatureType> oldCallable = _other.m_callable;
		const size_t oldFirstWord = _other.m_buffer[0];

		get_functor()->cloneIntoV(_other.m_buffer, _other.m_callable);
		get_functor()->~BaseFunctorWrapper();

		m_callable = oldCallable;
		m_buffer[0] = oldFirstWord;
	}
	else
	{
		std::swap(m_buffer[0], _other.m_buffer[0]);
		std::swap(m_callable, _other.m_callable);
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
template<size_t k_otherBufferSize, typename>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::swap(basic_callback<ReturnT(ArgTs...),k_otherBufferSize>& _other)
{
	const bool selfLocal = is_local(m_callable.m_this);
	const bool otherLocal = _other.is_local(_other.m_callable.m_this);

	if (selfLocal && otherLocal)
	{
		const size_t otherFunctorSize = _other.get_functor()->getSizeV();
		const size_t selfFunctorSize = get_functor()->getSizeV();

		const bool cloneOtherIntoLocal = sizeof(_other.m_buffer) <= sizeof(m_buffer) || otherFunctorSize <= sizeof(m_buffer);
		const bool cloneSelfIntoLocal = sizeof(m_buffer) <= sizeof(_other.m_buffer) || selfFunctorSize <= sizeof(_other.m_buffer);

		if (cloneOtherIntoLocal && cloneSelfIntoLocal)
		{
			callback_detail::BasicCallable<SignatureType> swapCallable;
			size_t swapBuffer[std::max(sizeof(m_buffer), sizeof(_other.m_buffer)) / sizeof(size_t)];

			_other.get_functor()->cloneIntoV(swapBuffer, swapCallable);
			_other.get_functor()->~BaseFunctorWrapper();

			get_functor()->cloneIntoV(_other.m_buffer, _other.m_callable);
			get_functor()->~BaseFunctorWrapper();
			((callback_detail::BaseFunctorWrapper<SignatureType>*)swapBuffer)->cloneIntoV(m_buffer, m_callable);
		}
		else if (cloneSelfIntoLocal)
		{
			void*const memory = allocate(otherFunctorSize);
			_other.get_functor()->cloneIntoV(memory, m_callable);
			_other.get_functor()->~BaseFunctorWrapper();

			((callback_detail::BaseFunctorWrapper<SignatureType>*)m_buffer)->cloneIntoV(_other.m_buffer, _other.m_callable);
			m_buffer[0] = (size_t)memory;
		}
		else if (cloneOtherIntoLocal)
		{
			void*const memory = allocate(selfFunctorSize);
			get_functor()->cloneIntoV(memory, _other.m_callable);
			get_functor()->~BaseFunctorWrapper();

			((callback_detail::BaseFunctorWrapper<SignatureType>*)_other.m_buffer)->cloneIntoV(m_buffer, m_callable);
			_other.m_buffer[0] = (size_t)memory;
		}
		else
		{
			assert(false && "Both local buffers are larger then each other, this should not have happen.");
		}
	}
	else if (otherLocal)
	{
		const callback_detail::BasicCallable<SignatureType> oldCallable = m_callable;
		const size_t oldFirstWord = m_buffer[0];

		const size_t otherFunctorSize = _other.get_functor()->getSizeV();
		const bool cloneOtherIntoLocal = sizeof(_other.m_buffer) <= sizeof(m_buffer) || otherFunctorSize <= sizeof(m_buffer);
		if (cloneOtherIntoLocal)
		{
			_other.get_functor()->cloneIntoV(m_buffer, m_callable);
			_other.get_functor()->~BaseFunctorWrapper();
		}
		else
		{
			void*const memory = allocate(otherFunctorSize);
			_other.get_functor()->cloneIntoV(memory, m_callable);
			_other.get_functor()->~BaseFunctorWrapper();

			m_buffer[0] = (size_t)memory;
		}

		_other.m_callable = oldCallable;
		_other.m_buffer[0] = oldFirstWord;
	}
	else if (selfLocal)
	{
		const callback_detail::BasicCallable<SignatureType> oldCallable = _other.m_callable;
		const size_t oldFirstWord = _other.m_buffer[0];

		const size_t selfFunctorSize = get_functor()->getSizeV();
		const bool cloneSelfIntoLocal = sizeof(m_buffer) <= sizeof(_other.m_buffer) || selfFunctorSize <= sizeof(_other.m_buffer);

		if (cloneSelfIntoLocal)
		{
			get_functor()->cloneIntoV(_other.m_buffer, _other.m_callable);
			get_functor()->~BaseFunctorWrapper();
		}
		else
		{
			void*const memory = allocate(selfFunctorSize);
			get_functor()->cloneIntoV(memory, m_callable);
			get_functor()->~BaseFunctorWrapper();

			_other.m_buffer[0] = (size_t)memory;
		}

		m_callable = oldCallable;
		m_buffer[0] = oldFirstWord;
	}
	else
	{
		std::swap(m_buffer[0], _other.m_buffer[0]);
		std::swap(m_callable, _other.m_callable);
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::clear()
{
	if (has_functor())
	{
		const bool local = is_local(m_callable.m_this);

		void*const blankPtr = local ? m_buffer : (void*)m_buffer[0];
		((callback_detail::BaseFunctorWrapper<SignatureType>*)blankPtr)->~BaseFunctorWrapper();

		if (!local)
		{
			free(blankPtr);
		}
	}

	initialize_empty();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_clone_comparable() const
{
	return !has_functor() || get_functor()->canBeComparedV();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...), k_bufferSize>::has_functor() const
{
	return m_buffer[0] != 0;
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
const bwn::callback_detail::BaseFunctorWrapper<ReturnT(ArgTs...)>* bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::get_functor() const
{
	const void*const rawWrapperPtr = is_local(m_callable.m_this)
		? m_buffer
		: (const void*)(m_buffer[0]);

	return (const callback_detail::BaseFunctorWrapper<ReturnT(ArgTs...)>*)(rawWrapperPtr);
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void* bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::allocate(size_t _size)
{
	if (_size > sizeof(m_buffer))
	{
		void*const ptr = malloc(_size);
		m_buffer[0] = size_t(ptr);
		return ptr;
	}
	else
	{
		return m_buffer;
	}
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
bool bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::is_local(void* _ptr) const
{
	return _ptr >= m_buffer && _ptr < (((const char*)m_buffer) + sizeof(m_buffer));
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::initialize_empty()
{
	m_callable.m_this = nullptr;
	m_callable.m_method = nullptr;
	initialize_empty_buffer();
}

template<typename ReturnT, typename...ArgTs, size_t k_bufferSize>
void bwn::basic_callback<ReturnT(ArgTs...),k_bufferSize>::initialize_empty_buffer()
{
	memset(m_buffer, 0, sizeof(m_buffer));
}

template<typename SignatureT, size_t k_leftBufferIntPtrCount, size_t k_rightBufferIntPtrCount>
inline bool operator==(
	const bwn::basic_callback<SignatureT,k_leftBufferIntPtrCount>& _left,
	const bwn::basic_callback<SignatureT,k_rightBufferIntPtrCount>& _right)
{
	return _left.is_equal(_right);
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator==(const bwn::basic_callback<SignatureT,k_bufferSize>& _left, std::nullptr_t)
{
	return !_left.is_valid();
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator==(std::nullptr_t, const bwn::basic_callback<SignatureT,k_bufferSize>& _right)
{
	return !_right.is_valid();
}

template<typename SignatureT, size_t k_leftBufferIntPtrCount, size_t k_rightBufferIntPtrCount>
inline bool operator!=(
	const bwn::basic_callback<SignatureT,k_leftBufferIntPtrCount>& _left,
	const bwn::basic_callback<SignatureT,k_rightBufferIntPtrCount>& _right)
{
	return !_left.is_equal(_right);
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator!=(const bwn::basic_callback<SignatureT,k_bufferSize>& _left, std::nullptr_t)
{
	return _left.is_valid();
}
template<typename SignatureT, size_t k_bufferSize>
inline bool operator!=(std::nullptr_t, const bwn::basic_callback<SignatureT,k_bufferSize>& _right)
{
	return _right.is_valid();
}
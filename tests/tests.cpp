#define CATCH_CONFIG_MAIN 
#include "catch.hpp"

#include <cstddef>

#include "callback/callback.hpp"

namespace tests
{
	struct VirtualBaseClass
	{
		VirtualBaseClass() { ++s_instanceCount; }
		VirtualBaseClass(const VirtualBaseClass&) { ++s_instanceCount; }
		VirtualBaseClass(VirtualBaseClass&&) noexcept { ++s_instanceCount; }
		VirtualBaseClass& operator=(const VirtualBaseClass&) = default;
		VirtualBaseClass& operator=(VirtualBaseClass&&) noexcept = default;
		virtual ~VirtualBaseClass() { --s_instanceCount; }

		size_t virtualbase_call(size_t p_arg)
		{
			return p_arg + m_virtualbase_buffer + 1;
		}

		virtual size_t virtual_callV(size_t p_arg)
		{
			return p_arg + m_virtualbase_buffer + 2;
		}

		static int32_t s_instanceCount;

		size_t m_virtualbase_buffer = 10;
	};

	struct FirstBaseClass : public virtual VirtualBaseClass
	{
		FirstBaseClass() { ++s_instanceCount; }
		FirstBaseClass(const FirstBaseClass&) { ++s_instanceCount; }
		FirstBaseClass(FirstBaseClass&&) noexcept { ++s_instanceCount; }
		FirstBaseClass& operator=(const FirstBaseClass&) = default;
		FirstBaseClass& operator=(FirstBaseClass&&) noexcept = default;
		virtual ~FirstBaseClass() { --s_instanceCount; }

		size_t firstbase_call(size_t p_arg)
		{
			return p_arg + m_firstbase_buffer + 3;
		}

		virtual size_t first_callV(size_t p_arg)
		{
			return p_arg + m_firstbase_buffer + 4;
		}

		virtual size_t virtual_callV(size_t p_arg) override
		{
			return p_arg + m_firstbase_buffer + 5;
		}

		static int32_t s_instanceCount;

		size_t m_firstbase_buffer = 100;
	};

	struct SecondBaseClass : public virtual VirtualBaseClass
	{
		SecondBaseClass() { ++s_instanceCount; }
		SecondBaseClass(const SecondBaseClass&) { ++s_instanceCount; }
		SecondBaseClass(SecondBaseClass&&) noexcept { ++s_instanceCount; }
		SecondBaseClass& operator=(const SecondBaseClass&) = default;
		SecondBaseClass& operator=(SecondBaseClass&&) noexcept = default;
		virtual ~SecondBaseClass() { --s_instanceCount; }

		size_t secondbase_call(size_t p_arg)
		{
			return p_arg + m_secondbase_buffer + 6;
		}

		virtual size_t second_callV(size_t p_arg)
		{
			return p_arg + m_secondbase_buffer + 7;
		}

		virtual size_t virtual_callV(size_t p_arg) override
		{
			return p_arg + m_secondbase_buffer + 8;
		}

		static int32_t s_instanceCount;

		size_t m_secondbase_buffer = 1000;
	};

	struct DerivedClass : public FirstBaseClass, public SecondBaseClass
	{
		DerivedClass() { ++s_instanceCount; }
		DerivedClass(const DerivedClass&) { ++s_instanceCount; }
		DerivedClass(DerivedClass&&) noexcept { ++s_instanceCount; }
		DerivedClass& operator=(const DerivedClass&) = default;
		DerivedClass& operator=(DerivedClass&&) noexcept = default;
		virtual ~DerivedClass() { --s_instanceCount; }

		size_t derived_call(size_t p_arg)
		{
			return p_arg + m_derived_buffer + 9;
		}

		virtual size_t first_callV(size_t p_arg) override
		{
			return p_arg + m_derived_buffer + 10;
		}

		virtual size_t second_callV(size_t p_arg) override
		{
			return p_arg + m_derived_buffer + 11;
		}

		virtual size_t virtual_callV(size_t p_arg) override
		{
			return p_arg + m_derived_buffer + 12;
		}

		static int32_t s_instanceCount;

		size_t m_derived_buffer = 10000;
	};

	size_t globalFunc(size_t p_arg)
	{
		return p_arg + 13;
	}

	struct SmallFunctor
	{
		SmallFunctor() { ++s_instanceCount; }
		SmallFunctor(const SmallFunctor&) { ++s_instanceCount; }
		SmallFunctor(SmallFunctor&&) noexcept { ++s_instanceCount; }
		SmallFunctor& operator=(const SmallFunctor&) = default;
		SmallFunctor& operator=(SmallFunctor&&) noexcept = default;
		~SmallFunctor() { --s_instanceCount; }

		bool operator==(const SmallFunctor& p_other) const
		{
			return m_functor_buffer == p_other.m_functor_buffer;
		}

		size_t operator()(size_t p_arg)
		{
			return p_arg + m_functor_buffer + 14;
		}

		static int32_t s_instanceCount;

		size_t m_functor_buffer = 100000;
	};

	struct BigFunctor
	{
		BigFunctor() { ++s_instanceCount; }
		BigFunctor(const BigFunctor&) { ++s_instanceCount; }
		BigFunctor(BigFunctor&&) noexcept { ++s_instanceCount; }
		BigFunctor& operator=(const BigFunctor&) = default;
		BigFunctor& operator=(BigFunctor&&) noexcept = default;
		~BigFunctor() { --s_instanceCount; }

		bool operator==(const BigFunctor& p_other) const
		{
			return m_functor_buffer == p_other.m_functor_buffer;
		}

		size_t operator()(size_t p_arg)
		{
			return p_arg + m_functor_buffer[9] + 14;
		}

		static int32_t s_instanceCount;

		size_t m_functor_buffer[10] {};
	};

	int32_t VirtualBaseClass::s_instanceCount = 0;
	int32_t FirstBaseClass::s_instanceCount = 0;
	int32_t SecondBaseClass::s_instanceCount = 0;
	int32_t DerivedClass::s_instanceCount = 0;
	int32_t SmallFunctor::s_instanceCount = 0;
	int32_t BigFunctor::s_instanceCount = 0;

	using SmallCallback = ::bwn::basic_callback<size_t(size_t), sizeof(size_t)>;
	using BigCallback = ::bwn::basic_callback<size_t(size_t), sizeof(size_t) * 20>;

	void clearCounters()
	{
		VirtualBaseClass::s_instanceCount = 0;
		FirstBaseClass::s_instanceCount = 0;
		SecondBaseClass::s_instanceCount = 0;
		DerivedClass::s_instanceCount = 0;
		SmallFunctor::s_instanceCount = 0;
		BigFunctor::s_instanceCount = 0;
	}

} // namespace tests

TEST_CASE("Simple calling.")
{
	SECTION("Global function.")
	{
		tests::clearCounters();
		{
			const tests::SmallCallback callback( &tests::globalFunc );
			REQUIRE(callback(42) == tests::globalFunc(42));
		}
	}
	SECTION("Simple class function") 
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::SmallCallback callback( &object, &tests::VirtualBaseClass::virtualbase_call );
			REQUIRE(callback(42) == object.virtualbase_call(42));
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Simple class virtual function")
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::SmallCallback callback( &object, &tests::VirtualBaseClass::virtual_callV );
			REQUIRE(callback(42) == object.virtual_callV(42));
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Derived class virtual function")
	{
		tests::clearCounters();
		{
			tests::DerivedClass object;
			const tests::SmallCallback callback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			REQUIRE(callback(42) == object.virtual_callV(42));
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
		REQUIRE(tests::FirstBaseClass::s_instanceCount == 0);
		REQUIRE(tests::SecondBaseClass::s_instanceCount == 0);
		REQUIRE(tests::DerivedClass::s_instanceCount == 0);
	}
	SECTION("Small functor")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;
			const tests::SmallCallback callback( functor );
			REQUIRE(callback(42) == functor(42));
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}
	SECTION("Big functor")
	{
		tests::clearCounters();
		{
			tests::BigFunctor functor;
			const tests::SmallCallback callback( functor );
			REQUIRE(callback(42) == functor(42));
		}
		REQUIRE(tests::BigFunctor::s_instanceCount == 0);
	}
}

TEST_CASE("Copy construction calling.")
{
	SECTION("Global function.")
	{
		tests::clearCounters();
		{
			const tests::SmallCallback initialCallback( &tests::globalFunc );
			const tests::SmallCallback copyCallback( initialCallback );
			REQUIRE(initialCallback(42) == tests::globalFunc(42));
			REQUIRE(copyCallback(42) == tests::globalFunc(42));
			REQUIRE(initialCallback == copyCallback);
		}
	}
	SECTION("Simple class function") 
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::SmallCallback initialCallback( &object, &tests::VirtualBaseClass::virtualbase_call );
			const tests::SmallCallback copyCallback( initialCallback );
			REQUIRE(initialCallback(42) == object.virtualbase_call(42));
			REQUIRE(copyCallback(42) == object.virtualbase_call(42));
			REQUIRE(initialCallback == copyCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Simple class virtual function")
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::SmallCallback initialCallback( &object, &tests::VirtualBaseClass::virtual_callV );
			const tests::SmallCallback copyCallback( initialCallback );
			REQUIRE(initialCallback(42) == object.virtual_callV(42));
			REQUIRE(copyCallback(42) == object.virtual_callV(42));
			REQUIRE(initialCallback == copyCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Derived class virtual function")
	{
		tests::clearCounters();
		{
			tests::DerivedClass object;
			const tests::SmallCallback initialCallback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			const tests::SmallCallback copyCallback( initialCallback );
			REQUIRE(initialCallback(42) == object.virtual_callV(42));
			REQUIRE(copyCallback(42) == object.virtual_callV(42));
			REQUIRE(initialCallback == copyCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
		REQUIRE(tests::FirstBaseClass::s_instanceCount == 0);
		REQUIRE(tests::SecondBaseClass::s_instanceCount == 0);
		REQUIRE(tests::DerivedClass::s_instanceCount == 0);
	}
	SECTION("Small functor")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;
			const tests::SmallCallback initialCallback( functor );
			const tests::SmallCallback copyCallback( initialCallback );
			REQUIRE(initialCallback(42) == functor(42));
			REQUIRE(copyCallback(42) == functor(42));
			REQUIRE(initialCallback == copyCallback);
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}
	SECTION("Big functor")
	{
		tests::clearCounters();
		{
			tests::BigFunctor functor;
			const tests::SmallCallback initialCallback( functor );
			const tests::SmallCallback copyCallback( initialCallback );
			REQUIRE(initialCallback(42) == functor(42));
			REQUIRE(copyCallback(42) == functor(42));
			REQUIRE(initialCallback == copyCallback);
		}
		REQUIRE(tests::BigFunctor::s_instanceCount == 0);
	}
}

TEST_CASE("Move construction calling.")
{
	SECTION("Global function.")
	{
		tests::clearCounters();
		{
			tests::SmallCallback initialCallback( &tests::globalFunc );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == tests::globalFunc(42));
		}
	}
	SECTION("Simple class function") 
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			tests::SmallCallback initialCallback( &object, &tests::VirtualBaseClass::virtualbase_call );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtualbase_call(42));
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Simple class virtual function")
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			tests::SmallCallback initialCallback( &object, &tests::VirtualBaseClass::virtual_callV );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtual_callV(42));
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Derived class virtual function")
	{
		tests::clearCounters();
		{
			tests::DerivedClass object;
			tests::SmallCallback initialCallback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtual_callV(42));
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
		REQUIRE(tests::FirstBaseClass::s_instanceCount == 0);
		REQUIRE(tests::SecondBaseClass::s_instanceCount == 0);
		REQUIRE(tests::DerivedClass::s_instanceCount == 0);
	}
	SECTION("Small functor")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;
			tests::SmallCallback initialCallback( functor );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == functor(42));
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}
	SECTION("Big functor")
	{
		tests::clearCounters();
		{
			tests::BigFunctor functor;
			tests::SmallCallback initialCallback( functor );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == functor(42));
		}
		REQUIRE(tests::BigFunctor::s_instanceCount == 0);
	}
}

TEST_CASE("Move construction into bigger size calling.")
{
	SECTION("Global function.")
	{
		tests::clearCounters();
		{
			const tests::SmallCallback compareCallback( &tests::globalFunc );
			tests::SmallCallback initialCallback( &tests::globalFunc );
			const tests::BigCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == tests::globalFunc(42));
			REQUIRE(compareCallback == moveCallback);
		}
	}
	SECTION("Simple class function") 
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::SmallCallback compareCallback( &object, &tests::VirtualBaseClass::virtualbase_call );
			tests::SmallCallback initialCallback( &object, &tests::VirtualBaseClass::virtualbase_call );
			const tests::BigCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtualbase_call(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Simple class virtual function")
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::SmallCallback compareCallback( &object, &tests::VirtualBaseClass::virtual_callV );
			tests::SmallCallback initialCallback( &object, &tests::VirtualBaseClass::virtual_callV );
			const tests::BigCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtual_callV(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Derived class virtual function")
	{
		tests::clearCounters();
		{
			tests::DerivedClass object;
			const tests::SmallCallback compareCallback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			tests::SmallCallback initialCallback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			const tests::BigCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtual_callV(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
		REQUIRE(tests::FirstBaseClass::s_instanceCount == 0);
		REQUIRE(tests::SecondBaseClass::s_instanceCount == 0);
		REQUIRE(tests::DerivedClass::s_instanceCount == 0);
	}
	SECTION("Small functor")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;
			const tests::SmallCallback compareCallback( functor );
			tests::SmallCallback initialCallback( functor );
			const tests::BigCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == functor(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}
	SECTION("Big functor")
	{
		tests::clearCounters();
		{
			tests::BigFunctor functor;
			const tests::SmallCallback compareCallback( functor );
			tests::SmallCallback initialCallback( functor );
			const tests::BigCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == functor(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::BigFunctor::s_instanceCount == 0);
	}
}

TEST_CASE("Move construction into smaller size calling.")
{
	SECTION("Global function.")
	{
		tests::clearCounters();
		{
			const tests::BigCallback compareCallback( &tests::globalFunc );
			tests::BigCallback initialCallback( &tests::globalFunc );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == tests::globalFunc(42));
			REQUIRE(compareCallback == moveCallback);
		}
	}
	SECTION("Simple class function") 
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::BigCallback compareCallback( &object, &tests::VirtualBaseClass::virtualbase_call );
			tests::BigCallback initialCallback( &object, &tests::VirtualBaseClass::virtualbase_call );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtualbase_call(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Simple class virtual function")
	{
		tests::clearCounters();
		{
			tests::VirtualBaseClass object;
			const tests::BigCallback compareCallback( &object, &tests::VirtualBaseClass::virtual_callV );
			tests::BigCallback initialCallback( &object, &tests::VirtualBaseClass::virtual_callV );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtual_callV(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
	}
	SECTION("Derived class virtual function")
	{
		tests::clearCounters();
		{
			tests::DerivedClass object;
			const tests::BigCallback compareCallback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			tests::BigCallback initialCallback( (tests::VirtualBaseClass*)(&object), &tests::VirtualBaseClass::virtual_callV );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == object.virtual_callV(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::VirtualBaseClass::s_instanceCount == 0);
		REQUIRE(tests::FirstBaseClass::s_instanceCount == 0);
		REQUIRE(tests::SecondBaseClass::s_instanceCount == 0);
		REQUIRE(tests::DerivedClass::s_instanceCount == 0);
	}
	SECTION("Small functor")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;
			const tests::BigCallback compareCallback( functor );
			tests::BigCallback initialCallback( functor );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == functor(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}
	SECTION("Big functor")
	{
		tests::clearCounters();
		{
			tests::BigFunctor functor;
			const tests::BigCallback compareCallback( functor );
			tests::BigCallback initialCallback( functor );
			const tests::SmallCallback moveCallback( std::move(initialCallback) );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(moveCallback(42) == functor(42));
			REQUIRE(compareCallback == moveCallback);
		}
		REQUIRE(tests::BigFunctor::s_instanceCount == 0);
	}
}

TEST_CASE("Nested callbacks.")
{
	SECTION("Copy")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;

			tests::SmallCallback initialCallback( functor );
			const tests::BigCallback nestedCallback( [callback = initialCallback](size_t _number){ return callback(_number); } );
			REQUIRE(initialCallback.is_valid());
			REQUIRE(nestedCallback(42) == functor(42));
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}

	SECTION("Move")
	{
		tests::clearCounters();
		{
			tests::SmallFunctor functor;

			tests::SmallCallback initialCallback( functor );
			const tests::BigCallback nestedCallback( [callback = std::move(initialCallback)](size_t _number){ return callback(_number); } );
			REQUIRE(!initialCallback.is_valid());
			REQUIRE(nestedCallback(42) == functor(42));
		}
		REQUIRE(tests::SmallFunctor::s_instanceCount == 0);
	}
}